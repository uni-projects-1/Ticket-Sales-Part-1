var express = require("express");
var router = express.Router();
var ticketController = require("../controllers/Tickets");
var authController = require("../controllers/Auth");
var multer = require("multer");
var { body, validationResult } = require("express-validator");

var images = multer({ dest: "../public/images/tickets" });

// GET tickets dashboard if the user is logged in
router.get("/", authController.verifyToken, ticketController.dashboard);

// GET tickets form create if the user is logged in
router.get(
  "/formCreate",
  authController.verifyToken,
  ticketController.formCreate
);

router.get(
  "/create/error",
  authController.verifyToken,
  ticketController.formCreateError
)

router.get(
  "/formCreateEventAssociated/:eventName",
  authController.verifyToken,
  ticketController.formCreateEventAssociated
);

// POST ticket into the database based on the data of the form in the request made and if the user is logged in
router.post(
  "/create",
  images.single("image"),
  [
    body("name")
      .isLength({ min: 1 })
      .withMessage("Name is required"),
    body("date")
      .isDate()
      .withMessage("Date is required"),
    body("hour")
      .isTime()
      .withMessage("Hour is required"),
    body("row")
      .isNumeric()
      .withMessage("Row is required"),
    body("seat")
      .isNumeric()
      .withMessage("Seat is required"),
    body("type")
      .isIn(["VIP", "Normal", "Family", "Child"])
      .withMessage("Type is required"),
    body("price")
      .isNumeric()
      .withMessage("Price is required"),
  ],
  authController.verifyToken,
  // If the user is logged in, we save the image in the request
  ticketController.saveImage,
  function (req, res) {
    var errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.redirect("http://localhost:5000/admin/ticket/create/error");
    }

    // Create the ticket if there are no errors
    ticketController.create(req, res);
  }
);

router.get(
  "/getTicketsByEventName/:eventName",
  authController.verifyToken,
  ticketController.getTicketsByEventName
);

// GET ticket details if the user is logged in and the ticket exists
router.get(
  "/show/:ticketId",
  authController.verifyToken,
  ticketController.show
);

// GET ticket form edit if the user is logged in
router.get(
  "/formEdit/:ticketId",
  authController.verifyToken,
  ticketController.formEdit
);

// POST ticket into the database based on the data of the form in the request made and if the user is logged in
router.put(
  "/edit/:ticketId",
  images.single("image"),
  [
    body("name")
      .isLength({ min: 1 })
      .withMessage("Name is required"),
    body("date")
      .isDate()
      .withMessage("Date is required"),
    body("hour")
      .isTime()
      .withMessage("Hour is required"),
    body("row")
      .isNumeric()
      .withMessage("Row is required"),
    body("seat")
      .isNumeric()
      .withMessage("Seat is required"),
    body("type")
      .isIn(["VIP", "Normal", "Family", "Child"])
      .withMessage("Type is required"),
    body("price")
      .isNumeric()
      .withMessage("Price is required"),
  ],
  authController.verifyToken,
  ticketController.saveImage,
  function (req, res, next) {
    var errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    } else {
      // Edit the ticket if there are no errors
      ticketController.edit(req, res);
    }
  }
);

// DELETE ticket from the database if the user is logged in and is a manager
router.delete(
  "/delete/:ticketId",
  authController.verifyTokenManager,
  // If the user is logged in and is a manager, we delete the ticket
  ticketController.delete
);

module.exports = router;
