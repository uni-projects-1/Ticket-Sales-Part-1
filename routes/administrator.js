var express = require("express");
var router = express.Router();
var administratorController = require("../controllers/Administrator");
var authController = require("../controllers/Auth");
var { body, validationResult } = require("express-validator");

router.get(
  // GET the dashboard of the administrator
  "/",
  authController.verifyTokenManager, // Verify if the user is logged in and if the user is a manager
  administratorController.dashboard // If the user is logged in and is a manager, we call the method to render the dashboard
);

router.get(
  // GET the form to create a new administrator
  "/formCreate",
  authController.verifyTokenManager,
  administratorController.formCreate
);

router.get(
  "/create/error",
  authController.verifyTokenManager,
  administratorController.formCreateError
)

router.post(
  // POST the form to create a new administrator
  "/create",
  [
    body("name")
      .isLength({ min: 3, max: 50 })
      .matches(/[A-Za-zÀ-ÖØ-öø-ÿ]/)
      .withMessage("Employee's name is required!"),
    body("gender")
      .isLength({ min: 1, max: 1 })
      .withMessage("1")
      .isIn(["M", "F", "O", "m", "f", "o"])
      .withMessage("Employee's gender (M, F or O) is required!"),
    body("dob")
      .isDate()
      .withMessage("Employee's date of birth (yyyy-mm-dd) is required!"),
    body("cellPhone")
      .matches(/9[1236][0-9]{7}|2[1-9][0-9]{7}/)
      .withMessage("Employee's cell phone (9xxxxxxxx) is required!"),
    body("email")
      .isLength({ min: 3, max: 70 })
      .isEmail()
      .withMessage("Employee's email (xxx@xxx.xxx) is required!"),
    body("street")
      .isLength({ min: 3, max: 70 })
      .withMessage("Employee's street is required!"),
    body("city")
      .isLength({ min: 3, max: 50 })
      .withMessage("Employee's city is required!"),
    body("postalCode")
      .isPostalCode("any", { locale: "pt-PT" })
      .withMessage("Employee's postal code (xxxx-xxx) is required!"),
    body("administratorType")
      .isIn(["Manager", "manager", "Administrator", "administrator"])
      .withMessage("Employee's type (Manager or Administrator) is required!"),
    body("password")
      .isLength({ min: 3, max: 20 })
      .withMessage("Employee's password is required!"),
  ],

  authController.verifyToken,
  // Verify if the user is logged in
  function (req, res, next) {
    var errors = validationResult(req);
    // If there are errors in the validation of the form fields, we return the errors to the user
    if (!errors.isEmpty()) {
      return res.redirect("http://localhost:5000/admin/administrator/create/error")
    }
    // If the user is logged in, we call the method to create the administrator
    administratorController.create(req, res);
  }
);

router.get(
  // GET the form to edit an administrator
  "/show/:administratorId",
  authController.verifyTokenManager,
  administratorController.show
);

router.get(
  // GET the form to edit an administrator
  "/formEdit/:administratorId",
  authController.verifyTokenManager,
  administratorController.formEdit
);

router.put(
  // POST the form to edit an administrator
  "/edit/:administratorId",
  [
    body("name")
      .isLength({ min: 3, max: 50 })
      .matches(/[A-Za-zÀ-ÖØ-öø-ÿ]/)
      .withMessage("Employee's name is required!"),
    body("gender")
      .isLength({ min: 1, max: 1 })
      .withMessage("1")
      .isIn(["M", "F", "O", "m", "f", "o"])
      .withMessage("Employee's gender (M, F or O) is required!"),
    body("dob")
      .isDate()
      .withMessage("Employee's date of birth (yyyy-mm-dd) is required!"),
    body("cellPhone")
      .matches(/9[1236][0-9]{7}|2[1-9][0-9]{7}/)
      .withMessage("Employee's cell phone (9xxxxxxxx) is required!"),
    body("email")
      .isLength({ min: 3, max: 70 })
      .isEmail()
      .withMessage("Employee's email (xxx@xxx.xxx) is required!"),
    body("street")
      .isLength({ min: 3, max: 70 })
      .withMessage("Employee's street is required!"),
    body("city")
      .isLength({ min: 3, max: 50 })
      .withMessage("Employee's city is required!"),
    body("postalCode")
      .isPostalCode("any", { locale: "pt-PT" })
      .withMessage("Employee's postal code (xxxx-xxx) is required!"),
    body("administratorType")
      .isIn(["Manager", "manager", "Administrator", "administrator"])
      .withMessage("Employee's type (Manager or Administrator) is required!"),
    body("password")
      .isLength({ min: 3, max: 20 })
      .withMessage("Employee's password must be between 3 and 20 characters! "),
  ],
  // Verify if the user is logged in
  authController.verifyTokenManager,
  function (req, res, next) {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
      //console.log(errors.array());
      return res.status(422).json({ errors: errors.array() });
    } else {
      administratorController.edit(req, res);
    }
  }
);

// DELETE an administrator from the database if the user is logged in and is a manager
router.delete(
  "/delete/:administratorId",
  authController.verifyTokenManager,
  // If the user is logged in and is a manager, we delete the patrimony in the url
  administratorController.delete
);

module.exports = router; // Export the router to be used in the app.js file
