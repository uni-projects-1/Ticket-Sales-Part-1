var express = require('express');
var router = express.Router();
var pointsStore = require('../../controllers/store/PointsStore');

router.get("/getDiscountPer100Points", pointsStore.getDiscountPer100Points);

module.exports = router;