var express = require('express');
var router = express.Router();
var clientStore = require('../../controllers/store/ClientStore');
var { body, validationResult } = require('express-validator');

router.post("/login", clientStore.loginClient);

router.post(
    "/register",
    [
        body("name")
            .isLength({ min: 3, max: 60 })
            .matches(/[A-Za-zÀ-ÖØ-öø-ÿ]/)
            .withMessage("Client's name is required!"),
        body("gender")
            .isLength({ min: 1, max: 1 })
            .withMessage("1")
            .isIn(["M", "F", "O", "m", "f", "o"])
            .withMessage("Client's gender (M, F or O) is required!"),
        body("dob")
            .isDate()
            .withMessage("Clients's date of birth (yyyy-mm-dd) is required!"),
        body("cellPhone")
            .matches(/9[1236][0-9]{7}|2[1-9][0-9]{7}/)
            .withMessage("Clients's cell phone (9xxxxxxxx) is required or Clients's cell phone (2xxxxxxxx) is required!"),
        body("email")
            .isLength({ min: 3, max: 70 })
            .isEmail()
            .withMessage("Client's email (xxx@xxx.xxx) is required!"),
        body("address")
            .isLength({ min: 3, max: 70 })
            .withMessage("Clients's street is required!"),
        body("city")
            .isLength({ min: 3, max: 50 })
            .withMessage("Clients's city is required!"),
        body("postalCode")
            .isPostalCode("any", { locale: "pt-PT" })
            .withMessage("Clients's postal code (xxxx-xxx) is required!"),
        body("nif")
            .isLength(9)
            .withMessage("Client's cell phone (9xxxxxxxx) is required!"),
        body("password")
            .isLength({ min: 3 })
            .withMessage("Client's password is required!"),
    ],
    function (req, res) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ status: 422, errors: errors.array()[0].msg });
        }

        clientStore.registerClient(req, res);
    }
);

router.get("/getClient", clientStore.verifyToken, clientStore.getClient);

router.put(
    "/updateClient",
    [
        body("name")
            .isLength({ min: 3, max: 60 })
            .matches(/[A-Za-zÀ-ÖØ-öø-ÿ]/)
            .withMessage("Client's name is required!"),
        body("gender")
            .isLength({ min: 1, max: 1 })
            .withMessage("1")
            .isIn(["M", "F", "O", "m", "f", "o"])
            .withMessage("Client's gender (M, F or O) is required!"),
        body("dob")
            .isDate()
            .withMessage("Clients's date of birth (yyyy-mm-dd) is required!"),
        body("cellPhone")
            .matches(/9[1236][0-9]{7}|2[1-9][0-9]{7}/)
            .withMessage("Clients's cell phone (9xxxxxxxx) is required or Clients's cell phone (2xxxxxxxx) is required!"),
        body("email")
            .isLength({ min: 3, max: 70 })
            .isEmail()
            .withMessage("Client's email (xxx@xxx.xxx) is required!"),
        body("address")
            .isLength({ min: 3, max: 70 })
            .withMessage("Clients's street is required!"),
        body("city")
            .isLength({ min: 3, max: 50 })
            .withMessage("Clients's city is required!"),
        body("postalCode")
            .isPostalCode("any", { locale: "pt-PT" })
            .withMessage("Clients's postal code (xxxx-xxx) is required!"),
        body("nif")
            .isLength(9)
            .withMessage("Client's cell phone (9xxxxxxxx) is required!"),
    ],
    clientStore.verifyToken,
    function (req, res) {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).send({ status: 422, errors: errors.array() });
        }

        clientStore.updateClient(req, res);
    }
);

module.exports = router;