var express = require('express');
var router = express.Router();
var eventStore = require('../../controllers/store/EventsStore');

router.get("/getAllEvents", eventStore.getAllEvents);

router.get("/getEventByName/:name", eventsStoreController.getEventByName);

router.get("/getRelatedEvents/:name", eventsStoreController.getRelatedEvents);

router.get("/getClosestEventsToToday", eventsStoreController.getFiveClosestEventsToCurrentDate);

module.exports = router;