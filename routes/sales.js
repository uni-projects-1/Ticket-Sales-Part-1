var express = require("express");
var router = express.Router();
var salesController = require("../controllers/Sales");
var authController = require("../controllers/Auth");

/* SALES NEW AND USED BOOKS FROM BOOKSTORE TO CLIENT */

//GET the index of sales and purchases mode dashboard
router.get("/", salesController.dashboard);

//GET the page of registering a buy from a client
router.get("/formCreate", salesController.formCreate);

//POST request to make a registration of a sale
router.post(
  "/create",
  authController.verifyToken,
  salesController.clientPhase,
  salesController.ticketsPhase,
  salesController.pointsPhase,
  salesController.decisionPhase
);

//GET shows the information about a sale registered in backOffice
router.get("/show/:salesId", salesController.showSale);

//GET shows the information about a sale registered in backOffice
router.get("/get/:salesId", salesController.getSale);

//GET generate the invoice pdf document of a sale
router.get("/generateInvoicePdf/:salesId", salesController.generateInvoicePdf);

module.exports = router;
