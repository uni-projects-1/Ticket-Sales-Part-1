var express = require("express");
var router = express.Router();
var authController = require("../controllers/Auth");

router.get("/", authController.verifyToken, authController.dashboard);

// GET login form 
router.get("/login", authController.formLogin);

// GET login form with error message
router.get("/login/error", authController.errorLogin);

// POST login form
router.post("/login", authController.login);

// GET logout the user
router.get("/logout", authController.logout);

module.exports = router;