var mongoose = require("mongoose");

var AdministratorSchema = new mongoose.Schema({
  administratorId: Number,
  name: String,
  gender: String,
  dob: String,
  cellPhone: Number,
  email: String,
  street: String,
  city: String,
  postalCode: String,
  administratorType: String,
  password: String,
  updated_at: { type: Date, default: Date.now },
});

module.exports = mongoose.model("Administrator", AdministratorSchema);
