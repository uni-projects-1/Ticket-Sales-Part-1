var mongoose = require("mongoose");

var SaleSchema = new mongoose.Schema(
  {
    salesId: Number,

    client: {
      clientId: Number,
      name: String,
      nif: Number,
      cellPhone: Number,
      email: String,
      pointsBeforeSale: Number,
    },
    tickets: [
      {
        _id: String,
        ticketId: Number,
        name: String,
        local: String,
        date: Date,
        hour: Date,
        // Seat/Row combination of the ticket bought by the user
        eventSeat: {
          // ID of the row
          row: Number,
          // ID of the seat
          seat: Number,
        },
        type: String,
        quantity: Number,
        price: Number,
        imageTicket: {
          staticUrl: { type: "String" },
          type: { type: "String" },
        },
      },
    ],
    totalTicketsBought: Number,
    totalValue: Number,
    pointsToDiscount: Number,
    totalValueWithDiscount: Number,
    discountValuePer100Points: Number,
    date: {
      type: String,
      default: new Date().toISOString().slice(0, 10),
    },
  },
  {
    versionKey: false,
  }
);

module.exports = mongoose.model("Sale", SaleSchema);
