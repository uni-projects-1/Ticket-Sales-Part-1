var mongoose = require("mongoose");

/**
 * Define schema for clients in MongoDB database. The scheme has two main fields, customer personal
 * information and information related to customer loyalty to the box office
 */
var ClientSchema = new mongoose.Schema(
  {
    personalInformation: {
      clientId: Number,
      name: String,
      gender: String,
      dob: String,
      cellPhone: Number,
      email: { type: String, unique: true },
      password: String,
      address: String,
      city: String,
      postalCode: String,
      nif: { type: String, unique: true },
    },

    loyaltySystem: {
      creationMemberDate: {
        type: String,
        default: new Date().toISOString().slice(0, 10),
      },
      ticketsPurchased: { type: Number, default: 0 },
      totalMoneyPurchases: { type: Number, default: 0 },
      actualPoints: { type: Number, default: 0 },
    },
  },
  {
    versionKey: false,
  }
);

module.exports = mongoose.model("Client", ClientSchema);
