var mongoose = require("mongoose");
const { log } = require("console");
const { resolve } = require("path");
var Client = require("../models/Client.js");
var Sales = require("../models/Sales.js");
var clientController = {};
var bcrypt = require("bcryptjs");

clientController.dashboard = (req, res, next) => {
  // Find all clients in the database
  Client.find({}).then((dbClients) => {
    if (dbClients == null) {
      // If there is an error, print it
      return res.status(500).send({
        success: false,
        message: "Error retrieving clients",
      });
    } else {
      // If there is no error, render the dashboard page
      res.render("../views/template", {
        loadContent: {
          page: "../views/clients/dashboardClient.ejs",
          clients: dbClients,
        },
      });
    }
  });
};

//show form to create one client
clientController.formCreate = (req, res) => {
  res.render("../views/template", {
    loadContent: {
      page: "../views/clients/createClient.ejs",
      error: false,
    },
  });
};

//show form to create one client with error indication
clientController.formCreateError = (req, res) => {
  res.render("../views/template", {
    loadContent: {
      page: "../views/clients/createClient.ejs",
      error: true,
    },
  });
};

//Creates an Client
clientController.create = (req, res, next) => {
  // If there is a password, encrypt it
  if (req.body.password != null) {
    req.body.password = bcrypt.hashSync(req.body.password, 8);
  } else {
    req.body.password = "";
  }

  // Counts the number of clients in the database
  Client.countDocuments({}).then((countClients) => {
    let client = new Client({
      personalInformation: {
        clientId: countClients + 1,
        name: req.body.name,
        gender: req.body.gender,
        dob: req.body.dob,
        cellPhone: req.body.cellPhone,
        email: req.body.email,
        password: req.body.password,
        address: req.body.address,
        city: req.body.city,
        postalCode: req.body.postalCode,
        nif: req.body.nif,
      },
    });

    //Checks if the email, nif and clientId are unique in DB
    Promise.all([
      Client.findOne({ "personalInformation.clientId": client.clientId }),
      Client.findOne({ "personalInformation.email": client.email }),
      Client.findOne({ "personalInformation.nif": client.nif }),
    ]).then(([dbClientId, dbClientEmail, dbClientNif]) => {
      if (dbClientId != null || dbClientEmail != null || dbClientNif != null) {
        return res.redirect("http://localhost:5000/admin/client/create/error");
      } else {
        client.save().then(() => {
          res.redirect("/admin/client");
        });
      }
    });
  });
};

//Edit Client
clientController.edit = (req, res) => {
  //Checks if the email is valid, in other words, if its the same as before or new but unique in DB
  let checkDuplicatedEmail = new Promise((resolve, reject) => {
    Client.find({ "personalInformation.email": req.body.email }).then(
      (client) => {
        if (
          (client.length != 0 &&
            client[0].personalInformation.clientId == req.params.clientId) ||
          client.length == 0
        ) {
          resolve(); //The email is valid
        } else {
          reject(); //The email cannot be used
        }
      }
    );
  });

  checkDuplicatedEmail
    .then(async () => {
      let previousClientInfo = await Client.findOne({ "personalInformation.clientId": req.params.clientId })
      let previousPassword = previousClientInfo.personalInformation.password
      if (req.body.password != null) {
        req.body.password = bcrypt.hashSync(req.body.password, 8);
      } else {
        req.body.password = previousPassword;
      }

      Client.findOneAndUpdate(
        { "personalInformation.clientId": req.params.clientId },
        {
          $set: {
            personalInformation: {
              clientId: req.params.clientId,
              name: req.body.name,
              gender: req.body.gender,
              age: req.body.age,
              dob: req.body.dob,
              cellPhone: req.body.cellPhone,
              email: req.body.email,
              password: req.body.password,
              address: req.body.address,
              city: req.body.city,
              postalCode: req.body.postalCode,
              nif: req.body.nif,
            },
          },
        },
        { new: true }
      ).then(
        //says to return the updated item
        (editedClient) => {
          if (editedClient == null) {
            return res.status(500).send({
              success: false,
              message: "Error editing client!",
            });
          } else {
            res.status(200).send({ success: true, message: "Client edited!" });
          }
        }
      );
    })
    .catch(() => {
      res.status(500).send({ success: false, message: "Email already in use" });
    });
};

//shows the form edit
clientController.formEdit = (req, res, next) => {
  Client.findOne({ "personalInformation.clientId": req.params.clientId }).then(
    (dbClient) => {
      if (dbClient == null) {
        // If he doesn't exist, show an error
        res.render("../views/template", {
          loadContent: {
            page: "../views/clients/createClient.ejs",
            error: true,
          },
        });
      } else {
        res.render("../views/template", {
          loadContent: {
            page: "../views/clients/editClient.ejs",
            client: dbClient,
          },
        });
      }
    }
  );
};

clientController.getClientById = (req, res, next) => {
  // Find the client with the given id in the url
  Client.findOne({ "personalInformation.clientId": req.params.clientId }).then(
    (dbClient) => {
      if (dbClient == null) {
        return res.status(500).send({
          success: false,
          message: "Client not found",
        });
      } else {
        return res.status(200).json(dbClient);
      }
    }
  );
};

//View one Client
clientController.show = (req, res, next) => {
  Client.findOne({ "personalInformation.clientId": req.params.clientId }).then(
    (dbClient) => {
      if (dbClient == null) {
        return res.status(500).send({
          success: false,
          message: "Client not found",
        });
      } else {
        res.render("template", {
          loadContent: {
            page: "../views/clients/viewClient.ejs",
            client: dbClient,
          },
        });
      }
    }
  );
};

//checks if a client have at least one sale associated
checkIfClientExitsInSales = (nifClient) => {
  return new Promise((resolve, reject) => {
    Sales.exists({ "client.nif": nifClient }).then((exists) => {
      if (exists != null) {
        reject(exists);
      } else {
        if (exists) {
          resolve(true);
        } else {
          resolve(false);
        }
      }
    });
  });
};

decrementId = (req, res) => {
  // Find all the clients with a higher id than the one that is going to be deleted
  // and decrement their id by one
  return new Promise((resolve, reject) => {
    Client.find({
      "personalInformation.clientId": { $gt: req.params.clientId },
    }).then((clients) => {
      // For each client, decrement the id by one
      clients.forEach((client) => {
        client.personalInformation.clientId =
          client.personalInformation.clientId - 1;
        client.save();
      });
      // Resolve the promise after all the clients have been updated
      resolve();
    });
  });
};

//Delete Client
clientController.delete = (req, res, next) => {
  Promise.all([
    checkIfClientExitsInSales(req.params.clientId),
    decrementId(req, res),
  ])
    .then((result) => {
      if (result[0]) {
        res.status(400).json({
          error: "clientHavePuschasesAssociated",
        });
      } else {
        Client.deleteOne({
          "personalInformation.clientId": req.params.clientId,
        }).then((result) => {
          if (result == null) {
            res.status(500).json({
              error: "clientNotFound",
            });
          } else {
            res.status(200).json(result);
          }
        });
      }
    })
    .catch((err) => {
      next(err);
    });
};

module.exports = clientController;
