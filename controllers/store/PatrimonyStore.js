var mongoose = require('mongoose');
var Patrimony = require("../../models/Patrimony.js");

patrimonyStoreController = {};

patrimonyStoreController.getAllPatrimonies = function (req, res) {
    Patrimony.find({}).then(function (patrimonies) {
        if (!patrimonies) {
            return res.status(404).send({ message: 'Patrimonies not found' });
        }
        return res.json(patrimonies);
    });
};

module.exports = patrimonyStoreController;
