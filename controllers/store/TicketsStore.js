var mongoose = require('mongoose');
var Ticket = require("../../models/Tickets.js");

var ticketsStoreController = {};

ticketsStoreController.getTicketsForEvent = function (req, res) {
    Ticket.find({ name: req.params.eventName }).then(function (tickets) {
        if (!tickets) {
            return res.status(404).send({ message: 'Tickets not found' });
        }

        // Sort tickets by price, descending, so the most expensive tickets are first
        tickets = tickets.sort(function (a, b) {
            return b.price - a.price;
        });

        return res.json(tickets);
    });
};

module.exports = ticketsStoreController;