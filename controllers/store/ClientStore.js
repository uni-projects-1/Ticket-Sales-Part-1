var mongoose = require('mongoose');
var Client = require("../../models/Client.js");
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var dotenv = require("dotenv");
dotenv.config({ path: "./config.env" });

var clientsStoreController = {};

clientsStoreController.verifyToken = function (req, res, next) {
    // Check if the token is in the request header
    var token = req.headers['x-access-token'];

    if (!token) {
        return res.status(401).send({ message: 'No token provided' });
    }

    // If there is a token, we verify it using the secret key and the clientId
    jwt.verify(token, `${process.env.SECRET}`, function (err, decoded) {
        if (err) {
            return res.status(500).send({ message: 'Failed to authenticate token' });
        } else {
            // If the token is valid, we save the clientId in the request and call next()
            req.clientId = decoded.clientId;
            next();
        }
    });
};

clientsStoreController.loginClient = function (req, res) {
    // When the 
    Client.findOne({ "personalInformation.email": req.body.email }).then(function (client) {
        // If a client is found with this e-mail address, we check if the password is correct
        if (!client) {
            return res.status(404).send({ message: 'Client not found' });
        } else {
            // The password saved in the database is hashed, so we need to hash the password sent by the client
            // and compare the two hashed passwords
            var hashedPassword = bcrypt.hashSync(req.body.password, 8);

            let samePassword = bcrypt.compareSync(req.body.password, client.personalInformation.password);

            if (!samePassword) {
                return res.status(401).send({ message: 'Wrong password' });
            }

            // If the password is correct, we create a token for the client that expires in 4 hours and send it to the client
            var token = jwt.sign({ clientId: client.personalInformation.clientId }, `${process.env.SECRET}`, { expiresIn: 14400 });

            return res.json({ token: token, name: client.personalInformation.name });
        }
    })
}

clientsStoreController.registerClient = async function (req, res) {
    // Frontend is going to send the client data in the body of the request
    let numberOfClients = await Client.countDocuments();

    var client = new Client({
        personalInformation: {
            clientId: numberOfClients + 1,
            name: req.body.name,
            gender: req.body.gender,
            dob: req.body.dob,
            cellPhone: req.body.cellPhone,
            email: req.body.email,
            password: req.body.password,
            address: req.body.address,
            city: req.body.city,
            postalCode: req.body.postalCode,
            nif: req.body.nif,
        },
    });

    // If a client is found with this e-mail address, we return an error
    // If not, we save the client in the database
    Promise.all([
        Client.findOne({ "personalInformation.clientId": client.personalInformation.clientId }),
        Client.findOne({ "personalInformation.email": client.personalInformation.email }),
        Client.findOne({ "personalInformation.nif": client.personalInformation.nif }),
    ]).then(([dbClientId, dbClientEmail, dbClientNif]) => {
        if (dbClientId != null || dbClientEmail != null || dbClientNif != null) {
            return res.status(409).send({ status: 409, message: 'Client already exists' });
        } else {
            // Hash the password before saving it in the database
            client.personalInformation.password = bcrypt.hashSync(client.personalInformation.password, 8);

            // Save the client in the database
            client.save().then(function (client) {
                if (!client) {
                    return res.status(500).send({ status: 500, message: 'Failed to create client' });
                } else {
                    return res.status(200).json(client);
                }
            });
        }
    });
};

clientsStoreController.getClient = function (req, res) {
    // We get the clientId from the request
    var clientId = req.clientId;

    // We search for the client in the database
    Client.findOne({ "personalInformation.clientId": clientId }).then(function (client) {
        if (!client) {
            return res.status(404).send({ message: 'Client not found' });
        } else {
            return res.status(200).json(client);
        }
    });
};

clientsStoreController.updateClient = async function (req, res) {
    // We get the clientId from the request
    var clientId = req.clientId;

    // See if the password sent in the request exists
    // If it exists, we hash it and save it in the database
    // If it doesn't exist, we use the previous password

    if (req.body.password != null || req.body.password != undefined || req.body.password != "") {
        req.body.password = bcrypt.hashSync(req.body.password, 8);
    } else {
        let previousClient = await Client.findOne({ "personalInformation.clientId": clientId });
        req.body.password = previousClient.personalInformation.password;
    }

    Client.findOneAndUpdate(
        { "personalInformation.clientId": clientId },
        {
            $set: {
                personalInformation: {
                    clientId: req.clientId,
                    name: req.body.name,
                    gender: req.body.gender,
                    age: req.body.age,
                    dob: req.body.dob,
                    cellPhone: req.body.cellPhone,
                    email: req.body.email,
                    password: req.body.password,
                    address: req.body.address,
                    city: req.body.city,
                    postalCode: req.body.postalCode,
                    nif: req.body.nif,
                },
            },
        },
        { new: true }).then(function (client) {
            if (!client) {
                return res.status(500).send({ status: 500, message: 'Failed to update client' });
            } else {
                // If the client is found, we return it
                // So the front-end can update the client data
                // as the client sends the updated data in the body of the request
                return res.status(200).send({ status: 200, client: client });
            }
        });
};

module.exports = clientsStoreController;