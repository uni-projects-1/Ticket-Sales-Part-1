var mongoose = require('mongoose');
var Point = require("../../models/Points.js");

pointsStoreController = {};

pointsStoreController.getDiscountPer100Points = function (req, res) {
    Point.findOne({}).then(function (dbPoints) {
        if (dbPoints == null) {
            return res.status(404).json({ message: "No points policy found" });
        }

        let discoutPer100Points = dbPoints.discountForEach100Points;

        return res.status(200).json({ status: 200, discountPer100Points: discoutPer100Points });
    });
}

module.exports = pointsStoreController;