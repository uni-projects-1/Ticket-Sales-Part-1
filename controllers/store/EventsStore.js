var mongoose = require('mongoose');
var Event = require("../../models/Events.js");
var Patrimony = require("../../models/Patrimony.js");

eventsStoreController = {};

// Get all events in the database
eventsStoreController.getAllEvents = function (req, res) {
    Event.find({}).then(function (dbEvents) {
        if (dbEvents == null) {
            return res.status(404).json({ message: "No events found" });
        }

        let notCompletedEvents = dbEvents.filter((event) => {
            return event.finished == false;
        });

        let events = notCompletedEvents.map(async (event) => {
            const imageUrl = await getImageOfAssociatedPatrimony(event.location);
            let tempEvent = event.toObject();
            tempEvent.imageUrl = imageUrl;
            return tempEvent;
        });

        Promise.all(events).then(function (events) {
            return res.status(200).json(events);
        });
    });
};

eventsStoreController.getEventByName = function (req, res) {
    Event.findOne({ name: req.params.name }).then(async function (dbEvent) {
        if (dbEvent == null) {
            return res.status(404).json({ message: "No event found" });
        }

        let imageUrl = await getImageOfAssociatedPatrimony(dbEvent.location);
        let tempEvent = dbEvent.toObject();
        tempEvent.imageUrl = imageUrl;
        return res.status(200).json(tempEvent);
    });
};

eventsStoreController.getRelatedEvents = async function (req, res) {
    let event = await Event.findOne({ name: req.params.name });

    if (event == null) {
        return res.status(404).json({ message: "No event found" });
    }

    Event.find({ type: event.type }).then(function (dbEvents) {
        if (dbEvents == null) {
            return res.status(404).json({ message: "No events found" });
        }

        let notCompletedEvents = dbEvents.filter((event) => {
            return event.finished == false;
        });

        // Get the names of the events
        let eventNames = notCompletedEvents.map((event) => {
            return event.name;
        });

        // Remove the name of the event that was searched
        eventNames = eventNames.filter((eventName) => {
            return eventName != req.params.name;
        });

        return res.status(200).json(eventNames);
    });
};

eventsStoreController.getFiveClosestEventsToCurrentDate = function (req, res) {
    // Find all events and see which ones are closest to the current date
    Event.find({}).then(function (dbEvents) {
        if (dbEvents == null || dbEvents.length == 0) {
            return res.status(404).json({ message: "No events found" });
        }

        let notCompletedEvents = dbEvents.filter((event) => {
            return event.finished == false;
        });

        // Add the image url of the patrimony associated to the event
        let events = notCompletedEvents.map(async (event) => {
            const imageUrl = await getImageOfAssociatedPatrimony(event.location);
            let tempEvent = event.toObject();
            tempEvent.imageUrl = imageUrl;
            return tempEvent;
        });

        // Wait for all the promises to be resolved
        Promise.all(events).then(function (events) {
            let currentDate = new Date();
            let closestEvents = [];

            // Create an array of objects containing the event and its difference from the current date
            let eventsWithDifferences = events.map((event) => {
                const eventDate = new Date(event.date);
                const difference = eventDate.getTime() - currentDate.getTime();
                return { event, difference };
            });

            // Remove the events that have already passed
            const filteredEventsWithDifferences = eventsWithDifferences.filter((item) => {
                return item.difference >= 0;
            });

            // Sort the array based on the differences in ascending order
            filteredEventsWithDifferences.sort((a, b) => a.difference - b.difference);

            // Extract the first 5 events from the sorted array
            // If there are less than 5 events, extract all of them anyways
            closestEvents = filteredEventsWithDifferences.slice(0, Math.min(5, filteredEventsWithDifferences.length)).map((item) => item.event);

            return res.status(200).json(closestEvents);
        }).catch(() => {
            res.status(500).json({ message: "Error getting events" });
        });
    });
};

function getImageOfAssociatedPatrimony(location) {
    return new Promise(function (resolve, reject) {
        // We want to get the url of the image of the patrimony associated to the event.
        // For each event that calls this method.

        // Get the patrimony associated to the event
        Patrimony.findOne({ patrimonyId: location }).then(function (patrimony) {
            if (patrimony != null) {
                // Get the image url of the patrimony
                resolve(patrimony.image.publicPath);
            }
        });
    });
}

module.exports = eventsStoreController;


