var Sales = require("../models/Sales");
var Client = require("../models/Client");
var Tickets = require("../models/Tickets");
var Points = require("../models/Points");
var Events = require("../models/Events");
var fs = require("fs");
var pdf = require("pdf-creator-node");
var path = require("path");

var salesController = {};

//updates the stock of Tickets in event  (ESTA ALTERADA !!)
function updateStockTickets(eventName, quantityTickets) {
  return new Promise((resolve, reject) => {
    Events.findOneAndUpdate(
      { name: eventName },
      {
        $inc: {
          seatsAvailable: -quantityTickets,
        },
      },
      { new: true }
    ).then((event) => {
      if (event == null) {
        reject();
      } else {
        resolve();
      }
    });
  });
}

//Update the loyalty information of a client after his purchase (ESTA ALTERADA !!!)
function updateLoyaltyClient(
  clientId,
  updatedPoints,
  plusTicketsPurchased,
  plusMoneyPurchased
) {
  return new Promise((resolve, reject) => {
    Client.findOneAndUpdate(
      clientId,
      {
        $inc: {
          "loyaltySystem.ticketsPurchased": +plusTicketsPurchased,
          "loyaltySystem.totalMoneyPurchases": +plusMoneyPurchased,
        },
        $set: {
          "loyaltySystem.actualPoints": updatedPoints,
        },
      },
      { new: true }
    ).then((client) => {
      if (client == null) {
        reject();
      } else {
        resolve();
      }
    });
  });
}

//Get the loyalty program of the store (ESTA ALTERADA !!!)
function getFidelizationProgram() {
  return new Promise((resolve, reject) => {
    Points.findOne({}).then((dbFidelizationProgram) => {
      if (dbFidelizationProgram == null) {
        reject();
      } else {
        resolve(dbFidelizationProgram);
      }
    });
  });
}

function getTicketsByEventName(req) {
  return new Promise((resolve, reject) => {
    Tickets.find({ name: req.body.eventName }).then((dbTickets) => {
      if (dbTickets == null) {
        reject();
      } else {
        resolve(dbTickets);
      }
    });
  });
}

function getEventByEventName(req) {
  return new Promise((resolve, reject) => {
    //console.log(req.body.eventName);
    Events.findOne({ name: req.body.eventName }).then((dbEvent) => {
      if (dbEvent == null) {
        reject();
      } else {
        resolve(dbEvent);
      }
    });
  });
}

//gets the form to register a purchase by client (ESTA ALTERADA !!!)
salesController.formCreate = async (req, res) => {
  let events = await Events.find({});
  let clients = await Client.find({});

  res.render("../views/template", {
    loadContent: {
      page: "../views/sales/formCreate",
      events: events,
      clients: clients,
    },
  });
};

//renders dashboard of sales  (ALTERADA !!!)
salesController.dashboard = async (req, res, next) => {
  /**
  let perPage = req.query.perPage || 10; //10 per default
  let page = req.query.page || 1; //page 1 in default

  //checks if the request was a custom search
  let searchType = req.query.searchType || "default";
  let searchValue = req.query.searchValue || null;

  //object for search
  let searchCustomization;

  //checks the search type
  try {
    switch (searchType) {
      case "NIF":
        searchCustomization = { "client.nif": Number(searchValue) };
        break;

      case "clientName":
        searchCustomization = {
          "client.name": {
            $regex: `.*${searchValue}.*`,
            $options: "i",
          }, //option i: search for upper or lower case
        };
        break;
      case "salesId":
        searchCustomization = {
          salesId: Number(searchValue),
        };
        break;

      default:
        searchCustomization = {};
        break;
    }
  } catch {
    //If some error in casting occurred
    searchCustomization = {};
    searchType = "default";
    searchValue = null;
  }

  Sales.find(searchCustomization).exec((err, dbSale) => {
    if (err) {
      return next(err);
    } else {
      let counter = dbSale.length;
      let totalPages = Math.ceil(counter / perPage);
      //if the page to search exceeds the max, the max is associated
      if (page > totalPages) {
        page = totalPages;
      }
      Sales.find(searchCustomization)
        .skip(perPage * page - perPage) //skips the first N elements
        .limit(perPage) //limits the number of results
        .exec(async (err, dbSale) => {
          if (err) {
            next(err);
          } else {
            res.render("../views/template", {
              loadContent: {
                page: "../views/sales/salesDashboard.ejs",
                sales: dbSale,
                pagination: {
                  totalPages: totalPages,
                  current: Number(page),
                  perPage: perPage,
                },
                search: {
                  type: searchType,
                  value: searchValue,
                },
              },
            });
          }
        });
    }
    
  });
  */
  Sales.find({}).then((dbSale) => {
    if (dbSale == null) {
      res.render("../views/template", {
        loadContent: {
          page: "../views/sales/dashboardSales.ejs",
        },
      });
    } else {
      res.render("../views/template", {
        loadContent: {
          page: "../views/sales/dashboardSales.ejs",
          sales: dbSale,
        },
      });
    }
  });
};

//Search for client and create a var in req or return error (ALTERADA ) - JOAO PAROU AQUI
salesController.clientPhase = (req, res, next) => {
  Client.findOne({ "personalInformation.nif": req.body.clientNif }).then(
    (dbClient) => {
      //debugger;
      if (dbClient == null) {
        res.status(400).send({
          success: false,
          message: "ClientNotFound",
        });
      } else {
        req.client = dbClient;
        //console.log(req.client);
        next();
        //console.log(req.client);
      }
    }
  );
};

//Search for tickets and adds the tickets rows to the local array tickets  (ALTERADA !!!)
salesController.ticketsPhase = async (req, res, next) => {
  // Check if the form has appropriate values for the tickets sent
  if (
    req.body.vipTickets < 0 ||
    req.body.normalTickets < 0 ||
    req.body.familyTickets < 0 ||
    req.body.childTickets < 0 ||
    req.body.vipTickets == null ||
    req.body.normalTickets == null ||
    req.body.familyTickets == null ||
    req.body.childTickets == null ||
    typeof req.body.vipTickets != "number" ||
    typeof req.body.normalTickets != "number" ||
    typeof req.body.familyTickets != "number" ||
    typeof req.body.childTickets != "number"
  ) {
    res.status(400).send({
      success: false,
      message: "InvalidTickets",
    });
  }

  var event = await getEventByEventName(req);

  if (req.body.totalTickets > event.seatsAvailable) {
    res.status(409).send({
      success: false,
      message: "NotEnoughSeats",
    });

    return;
  }

  var tickets = await getTicketsByEventName(req);
  var totalValue = 0;

  //debugger;

  //console.log(tickets);
  //console.log(event);

  let ticketsWithInfoForSale = [];

  //console.log(tickets);

  // For all tickets, we want to check if the quantity we're trying to sell is acceptable
  // by the event - Total amount of tickets we're trying to sell < amount of seats available for the event
  for (let i = 0; i < tickets.length; i++) {
    //debugger;
    try {
      let ticketObject = tickets[i];
      let ticket = ticketObject.toObject();
      let ticketType = ticket.type;

      let cardTotal = 0;
      ticketType == "VIP"
        ? (cardTotal += req.body.vipTickets * ticket.price)
        : ticketType == "Family"
          ? (cardTotal += req.body.familyTickets * ticket.price)
          : ticketType == "Normal"
            ? (cardTotal += req.body.normalTickets * ticket.price)
            : (cardTotal += req.body.childTickets * ticket.price);
      cardTotal = Number(cardTotal.toFixed(2));

      //if no errors, we can add the ticket to the array
      ticketType == "VIP"
        ? (ticket.quantity = req.body.vipTickets)
        : ticketType == "Family"
          ? (ticket.quantity = req.body.familyTickets)
          : ticketType == "Normal"
            ? (ticket.quantity = req.body.normalTickets)
            : (ticket.quantity = req.body.childTickets);

      ticketsWithInfoForSale.push(ticket);

      totalValue = totalValue + cardTotal;
      //console.log(totalValue);
    } catch (err) {
      //console.log(err);
    }
  }

  //debugger;

  req.totalTicketsBought = req.body.totalTickets;
  if (req.totalTicketsBought == 0) {
    res.status(400).send({
      success: false,
      message: "InvalidTickets",
    });

    return;
  }

  //adds the tickets to the req var
  req.tickets = ticketsWithInfoForSale;

  req.totalValue = totalValue.toFixed(2);

  next();
};

// (ALTERADA !!!)
salesController.pointsPhase = async (req, res, next) => {
  req.fidelizationProgram = await getFidelizationProgram();

  debugger;

  //console.log(1);
  //console.log(req.fidelizationProgram);
  //console.log(req.body);

  //console.log(req.fidelizationProgram);

  //console.log(req.body);
  let vipTicketPoints = req.fidelizationProgram.vipTicket * req.body.vipTickets;
  let familyTicketPoints =
    req.fidelizationProgram.familyTicket * req.body.familyTickets;
  let normalTicketPoints =
    req.fidelizationProgram.normalTicket * req.body.normalTickets;
  let childTicketPoints =
    req.fidelizationProgram.childrenTicket * req.body.childTickets;

  let totalPointsWithoutTicketsBonus =
    req.fidelizationProgram.earnedPointsForEachPurchaseTicket *
    req.body.totalTickets;

  // Calculate the points earned by for every 1€ spent from the total value
  //console.log(req.totalValue);
  let pointsEarned =
    Math.trunc(req.totalValue / 1) *
    req.fidelizationProgram.earnedPointsForEachEuro;

  // Calculate the total points earned in this sale
  let totalPoints =
    totalPointsWithoutTicketsBonus +
    vipTicketPoints +
    familyTicketPoints +
    normalTicketPoints +
    childTicketPoints +
    pointsEarned;

  //console.log(totalPoints);
  //console.log(
  //  "Total Points Earned Without Ticket Bonus:" + totalPointsWithoutTicketsBonus
  //);
  //console.log("VIP Ticket Points:" + vipTicketPoints);
  //console.log("Family Ticket Points:" + familyTicketPoints);
  //console.log("Normal Ticket Points:" + normalTicketPoints);
  //console.log("Child Ticket Points:" + childTicketPoints);
  //console.log("Points Earned:" + pointsEarned);
  req.pointsEarnedThisSale = totalPoints;

  req.discountValuePer100Points =
    req.fidelizationProgram.discountForEach100Points;

  // Check if the client has chosen to apply the discount for this sale
  // If not, skip the rest of the phase since we don't need to calculate the value to discount
  // and the points to remove from the client's current points
  if (!req.body.hasAppliedDiscount) {
    req.pointsLostThisSale = 0;
    req.valueToDiscount = 0;
  } else {
    // Calculate the value to discount based on the client's current points
    //console.log(req.client);
    let valueToMultiplyToPointsProgram = Math.trunc(
      req.client.loyaltySystem.actualPoints / 100
    );

    req.valueToDiscount =
      valueToMultiplyToPointsProgram *
      req.fidelizationProgram.discountForEach100Points;

    // Check if the value to discount is higher than the total value
    // If it is, the value to discount is the total value
    if (req.valueToDiscount > req.totalValue) {
      req.valueToDiscount = req.totalValue;
    }

    // Calculate the points to remove from the client's current points based on the value to discount
    let pointsToDiscount = valueToMultiplyToPointsProgram * 100;

    req.pointsLostThisSale = pointsToDiscount;

  }

  next();
};

//Last phase, checks if the process occurred without errors and make the response logic
salesController.decisionPhase = async (req, res, next) => {
  // Find the number of sales made and add 1
  // So we can create a unique sale id
  // and find the number of sales made more easily

  //debugger;
  let numberOfSales = await Sales.countDocuments();
  // Transform the strings stored in the array of tickets into objects
  // to be stored in the database

  let tickets = req.tickets.map((ticket) => JSON.stringify(ticket));

  let sale = new Sales({
    salesId: numberOfSales + 1,
    client: {
      clientId: req.client.personalInformation.clientId,
      name: req.client.personalInformation.name,
      nif: req.client.personalInformation.nif,
      cellPhone: req.client.personalInformation.cellPhone,
      email: req.client.personalInformation.email,
      pointsBeforeSale: req.client.loyaltySystem.actualPoints,
    },

    tickets: tickets, //array of tickets

    totalTicketsBought: req.totalTicketsBought,

    totalValue: req.totalValue,

    //for each will check if there is points trigger, if not just put 0
    pointsToDiscount:
      req.pointsLostThisSale != 0
        ? req.pointsLostThisSale //round to hundreds
        : 0,

    // Calculate the total value of the sale after the discount
    // If the discount is 0, the total value will not be changed

    totalValueWithDiscount: req.totalValue - req.valueToDiscount,

    discountValuePer100Points:
      req.discountValuePer100Points != 0 ? req.discountValuePer100Points : 0,
  });

  //debugger;

  //Update all the information of client, if its need
  try {
    // In case some error occurred, we can rollback the client's information
    var oldPoints = req.client.loyaltySystem.actualPoints;

    let totalPoints =
      req.client.loyaltySystem.actualPoints +
      req.pointsEarnedThisSale -
      req.pointsLostThisSale;

    let clientInfo = {
      "personalInformation.clientId": req.client.personalInformation.clientId,
    };

    await updateLoyaltyClient(
      clientInfo,
      totalPoints,
      req.totalTicketsBought,
      Number(Number(req.totalValue).toFixed(2))
    );
  } catch (err) {
    //console.log(err);
    res.status(500).send({
      success: false,
      message: "Error updating client information",
    });

    return;
  }

  try {
    //updates the stock in tickets
    let ticket;
    //console.log(req.tickets);

    for (let i = 0; i < req.tickets.length; i++) {
      ticket = req.tickets[i];
      //console.log(ticket);
      updateStockTickets(ticket.name, ticket.quantity);
    }
  } catch (err) {
    //if a error occurred in ticket, revert the changes in client
    await updateLoyaltyClient(
      req.client.personalInformation.clientId,
      oldPoints,
      -req.totalTicketsBought,
      -Number(Number(req.totalValue).toFixed(2))
    );

    res.status(500).send({
      success: false,
      message: "Error updating stock",
    });

    return;
  }

  //debugger;

  //Insert object in BD
  sale.save().then((dbSale) => {
    if (dbSale == null) {
      res.status(500).send({
        success: false,
        message: "Error creating sale",
      });
    } else {
      res.status(200).send({
        status: 200,
        success: true,
        message: "Sale created successfully",
        salesId: dbSale.salesId,
      });
    }
  });
};

//shows a sale  (ESTA ALTERADA !!!)
salesController.showSale = (req, res, err) => {
  Sales.findOne({ salesId: req.params.salesId }).then((sale) => {
    if (sale == null) {
      res.status(400).json({});
    } else {
      res.render("../views/template", {
        loadContent: {
          page: "../views/sales/viewSale.ejs",
          sale: sale,
        },
      });
    }
  });
};

//shows a sale (ESTÁ ALTERADA !!!)
salesController.getSale = (req, res) => {
  Sales.findOne({ salesId: req.params.salesId }).then((sale) => {
    if (sale == null) {
      res.status(400).json({});
    } else {
      res.status(200).json(sale);
    }
  });
};

//generate the invoice pdf document
salesController.generateInvoicePdf = (req, res, next) => {
  Sales.findOne({ salesId: req.params.salesId })
    .lean()
    .then((dbSale) => {
      if (dbSale == null) {
        //console.log("Error reading sale");
      } else {
        //call back function
        let cb = (err, html) => {
          if (err) {
            res.status(400).send("");
          }
          const filename = "fatura" + req.params.id + ".pdf";

          const document = {
            html: html,
            data: {
              host: req.get("host"),
              client: dbSale.client,
              books: dbSale.books,
              value: dbSale.totalValue,
              date: dbSale.date,
              valueWithDiscount: dbSale.totalValueWithDiscount,
              discount: dbSale.totalValue - dbSale.totalValueWithDiscount,
            },
            path: "./docs/faturas/" + filename,
          };

          pdf
            .create(document, options)
            .then((result) => {
              const filepath = "/docs/faturas/" + filename;
              res.status(200).send(filepath);
            })
            .catch((error) => {
              //console.log(error);
              res.status(400).send("");
            });
        };
        //check if the sale had discount
        dbSale.pointsToDiscount == 0
          ? fs.readFile(
            path.join(__dirname, "../styleInvoice/faturaTemplate.html"),
            "utf-8",
            (err, data) => {
              cb(err, data);
            }
          )
          : fs.readFile(
            path.join(
              __dirname,
              "../styleInvoice/faturaTemplateComDesconto.html"
            ),
            "utf-8",
            (err, data) => {
              cb(err, data);
            }
          );
      }
    });
};

module.exports = salesController;
