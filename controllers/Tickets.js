var mongoose = require("mongoose");
var fs = require("fs");
var Ticket = require("../models/Tickets.js");
const Event = require("../models/Events.js");
var path = require("path");
var sharp = require("sharp");

var ticketController = {};

ticketController.dashboard = function (req, res) {
  // Find all tickets in the database and render the view with all of them
  Ticket.find({}).then((dbTickets) => {
    if (dbTickets == null) {
      return res.status(500).send({
        success: false,
        message: "Error retrieving events",
      });
    } else {
      res.render("../views/template", {
        loadContent: {
          page: "../views/tickets/dashboardTicket.ejs",
          tickets: dbTickets,
        },
      });
    }
  });
};

ticketController.getTicketsByEventName = function (req, res) {
  // Find all tickets with the event name passed in the url and render the view with the tickets found
  Ticket.find({ name: req.params.eventName }).then((dbTickets) => {
    if (dbTickets == null) {
      //console.log("No tickets found for this event");
    } else {
      return res.status(200).json(dbTickets);
    }
  });
};

ticketController.show = function (req, res) {
  // Find the ticket with the id passed in the url and render the view with the ticket information
  Ticket.findOne({ ticketId: req.params.ticketId }).then((dbTicket) => {
    if (dbTicket == null) {
      //console.log("Error:", err);
    } else {
      res.render("../views/template", {
        loadContent: {
          page: "../views/tickets/viewTicket.ejs",
          ticket: dbTicket,
        },
      });
    }
  });
};

ticketController.formCreate = function (req, res) {
  // Find all events to send to the view under the name section as select options
  Event.find({}).then((dbEvents) => {
    if (dbEvents == null) {
      return res.status(500).send({
        success: false,
        message: "Error retrieving patrimonies",
      });
    } else {
      // Render the view to create a ticket with the events found
      res.render("../views/template", {
        loadContent: {
          page: "../views/tickets/createTicket.ejs",
          events: dbEvents,
          error: false,
        },
      });
    }
  });
};

ticketController.formCreateError = function (req, res) {
  // Find all events to send to the view under the name section as select options
  Event.find({}).then((dbEvents) => {
    if (dbEvents == null) {
      return res.status(500).send({
        success: false,
        message: "Error retrieving events",
      });
    } else {
      // Render the view to create a ticket with the events found
      res.render("../views/template", {
        loadContent: {
          page: "../views/tickets/createTicket.ejs",
          events: dbEvents,
          error: true,
        },
      });
    }
  });
};

ticketController.formCreateEventAssociated = function (req, res) {
  // Find all events to send to the view under the name section as select options
  Event.find({}).then((dbEvents) => {
    if (dbEvents == null) {
      return res.status(500).send({
        success: false,
        message: "Error retrieving patrimonies",
      });
    } else {
      // Render the view to create a ticket with the events found
      res.render("../views/template", {
        loadContent: {
          page: "../views/tickets/createTicketEventAssociated.ejs",
          events: dbEvents,
          name: req.params.eventName,
        },
      });
    }
  });
};

ticketController.saveImage = function (req, res, next) {
  let requestType = req.url.split("/")[1];
  requestType = requestType.charAt(0).toUpperCase() + requestType.slice(1);

  // If there is no file in the request, the creation of the ticket is canceled and the user is redirected to the form to create a ticket
  if (!req.file) {
    if (requestType == "Edit") {
      req.noNewImage = true;
      return next();
    } else {
      return res.redirect("/admin/ticket/formCreate");
    }
  } else {
    // If the image uploaded is not in the correct format, we return an error to the user
    if (
      req.file.mimetype != "image/jpeg" &&
      req.file.mimetype != "image/png" &&
      req.file.mimetype != "image/jpg"
    ) {
      return res.redirect("/admin/ticket/form" + requestType);
    }

    //debugger;
    // Gets the file extension of the image to add it to the name of the file
    var fileExtension = req.file.mimetype.split("/")[1];

    var destionationPath = req.file.destination;
    var ticketName = req.body.name;
    var resizedImagePath = path.join(destionationPath, ticketName + '.' + fileExtension);

    // Resize the image to 250x250 pixels
    sharp(req.file.path)
      .resize(250, 250)
      .toFile(resizedImagePath, function (err) {
        if (err) {
          //console.log(err);
          return res.redirect("/admin/ticket/form" + requestType);
        }

        // If resizing the image was successful, update req.body.publicPath with the path of the resized image
        req.body.publicPath = "/images/tickets/" + ticketName + '.' + fileExtension;

        // Delete the original image
        fs.unlink(req.file.path, function (err) {
          if (err) {
            //console.log(err);
          }
        });

        next();
      });
  }
};

ticketController.create = function (req, res) {
  // Count the number of tickets in the database and add 1 to the number to give the new ticket an id with that number
  Ticket.countDocuments({}).then((countTickets) => {
    let ticket = new Ticket({
      // ID of the ticket to be created
      ticketId: countTickets + 1,
      // Name of the ticket to be created
      name: req.body.name,
      // Local of the ticket to be created
      date: req.body.date,
      // Hour of the ticket to be created
      hour: req.body.hour,
      // EventSeat of the ticket to be created
      eventSeat: {
        // row of the eventSeat of the ticket to be created
        row: req.body.row,
        // column of the eventSeat of the ticket to be created
        seat: req.body.seat,
      },
      // Type of the ticket to be created
      type: req.body.type,
      // Price of the ticket to be created
      price: req.body.price,
      // Image of the ticket to be created
      image: {
        // publicPath of the image of the ticket to be created
        publicPath: req.body.publicPath,
        // typeOfFile of the image of the ticket to be created - jpg, png, jpeg.
        typeOfFile: req.body.mimetype,
      },
    });

    // Check if the ticket already exists in the database
    Promise.all([
      Ticket.findOne({ ticketId: ticket.ticketId }),
      Ticket.findOne({ name: ticket.name, type: ticket.type }),
    ]).then(([dbTickedId, dbTicketName]) => {
      if (dbTickedId != null || dbTicketName != null) {
        return res.redirect("http://localhost:5000/admin/ticket/create/error");
      } else {
        // If the ticket does not already exist in the database, it will be saved in the database
        ticket.save().then(() => {
          // Redirect to the view with all tickets after creating the ticket
          res.redirect("/admin/ticket");
        });
      }
    });
  });
};

function decrementId(req, res) {
  return new Promise((resolve, reject) => {
    // Find all tickets with id greater than the id of the ticket to be deleted
    // and decrement the id of each ticket by 1
    Ticket.find({ ticketId: { $gt: req.params.ticketId } }).then(
      (dbTickets) => {
        dbTickets.forEach((dbTicket) => {
          dbTicket.ticketId = dbTicket.ticketId - 1;
          dbTicket.save();
        });
        resolve();
      }
    );
  });
}

ticketController.delete = async function (req, res) {
  // Delete the ticket with the id passed in the url
  Ticket.findOneAndDelete({ ticketId: req.params.ticketId }).then(
    async (ticket) => {
      if (ticket == null) {
        // If no ticket is found, return error message
        return res
          .status(500)
          .send({ success: false, message: "Ticket not found" });
      } else {
        await decrementId(req, res);

        // Get the path of the image
        var path = __dirname + "/../public" + ticket.image.publicPath;
        // Remove the image from the public folder
        fs.unlink(path, function (err) {
          if (err) {
            //console.log(err);
          } else {
            //console.log("QR code of ticket removed successfully!");
          }
        });
        return res
          .status(200)
          .send({ success: true, message: "Ticket deleted" });
      }
    }
  );
};

ticketController.formEdit = async function (req, res) {
  // Find the ticket with the id passed in the url and render the view with the ticket information
  let dbTicket = await Ticket.findOne({ ticketId: req.params.ticketId })
  if (dbTicket == null) {
    return res.status(500).send({
      success: false,
      message: "Error retrieving ticket",
    });
  } else {
    debugger;
    // Find all events that don't already have 4 tickets made to send to the view under the name section as select options
    let tempEvents = await Event.find({});
    let nameFilter = await Ticket.find({});
    let events = [];

    // Filter the events that don't already have 4 tickets made
    for (let i = 0; i < tempEvents.length; i++) {
      let tempEvent = tempEvents[i];
      let tempTickets = nameFilter.filter(
        (ticket) => ticket.name == tempEvent.name
      );

      if (tempTickets.length < 4) {
        events.push(tempEvent);
      }
    }

    // Render the view to edit the ticket with the ticket information and events found
    res.render("../views/template", {
      loadContent: {
        page: "../views/tickets/editTicket.ejs",
        ticket: dbTicket,
        events: events,
      },
    });
  }
};

ticketController.edit = async function (req, res) {
  // Find the ticket with the id passed in the url
  //debugger;
  let previousTicket = await Ticket.findOne({ ticketId: req.params.ticketId });

  // If the user does not upload a new image, the previous image will be used

  //console.log(req.body);
  if (req.noNewImage) {
    // Get the ticket object of the previous ticket
    let previousTicketObject = previousTicket.toObject();
    // Get the image object of the previous ticket
    let previousTicketImage = previousTicketObject.image;
    // Get the path of the previous ticket image
    let previousTicketImagePath = previousTicketImage.publicPath;
    // Get the name of the previous ticket image so that it can be replaced with the new name
    let previousTicketImageName = previousTicketImagePath.split('/tickets/')[1].split('.')[0];
    // Replace the name of the previous ticket image with the new name
    previousTicketImagePath = previousTicketImagePath.replace(previousTicketImageName, req.body.name);
    // Replace the publicPath of the previous ticket image with the new publicPath
    //If the name of the ticket is not changed, the publicPath will remain the same
    previousTicketImage.publicPath = previousTicketImagePath;

    var newTicket = new Ticket(
      {
        // ID of the ticket to be updated
        ticketId: req.params.ticketId,
        // Name of the ticket to be updated
        name: req.body.name,
        // Date of the ticket to be updated
        date: req.body.date,
        // Hour of the ticket to be updated
        hour: req.body.hour,
        // EventSeat of the ticket to be updated
        eventSeat: {
          // row of the eventSeat of the ticket to be updated
          row: req.body.row,
          // column of the eventSeat of the ticket to be updated
          seat: req.body.seat,
        },
        // Type of the ticket to be updated
        type: req.body.type,
        // Price of the ticket to be updated
        price: req.body.price,
        // Image of the ticket to be updated
        image: previousTicketImage,
      },
      { _id: false }
    );
  } else {
    var newTicket = new Ticket(
      {
        // ID of the ticket to be updated
        ticketId: req.params.ticketId,
        // Name of the ticket to be updated
        name: req.body.name,
        // Date of the ticket to be updated
        date: req.body.date,
        // Hour of the ticket to be updated
        hour: req.body.hour,
        // EventSeat of the ticket to be updated
        eventSeat: {
          // row of the eventSeat of the ticket to be updated
          row: req.body.row,
          // column of the eventSeat of the ticket to be updated
          seat: req.body.seat,
        },
        // Type of the ticket to be updated
        type: req.body.type,
        // Price of the ticket to be updated
        price: req.body.price,
        // Image of the ticket to be updated
        image: {
          // Path of the image of the ticket to be updated
          publicPath: req.body.publicPath,
          // Type of file - Example: .jpg, .png, etc.
          typeOfFile: req.file.mimetype,
        }
      },
      { _id: false }
    );
  }

  let ticketsSameName = await Ticket.find({ name: newTicket.name });

  // Check if the ticket to be updated is in the array of tickets with the same name
  ticketsSameName = ticketsSameName.filter(
    (ticket) => ticket.ticketId != req.params.ticketId
  );

  // Find all tickets with the same name as the ticket to be updated
  // If there are already 4 tickets with the same name, return error message
  // This is done to prevent someone associating more than 4 tickets to the same event
  // for people that are sending requests to the server directly
  if (ticketsSameName.length >= 4) {
    res.status(500).send({
      success: false,
      message: "Error editing ticket! There are already 4 tickets for this event!",
    });

    return;
  };

  // Update the ticket with the new information
  Ticket.findOneAndUpdate({ ticketId: req.params.ticketId }, newTicket).then(
    (ticket) => {
      if (ticket == null) {
        // If no ticket is found, return error message
        return res.status(500).send({
          success: false,
          message: "Error editing ticket!",
        });
      } else {
        // Only change the name of the image if the name of the ticket is changed and the user does not upload a new image
        if (req.noNewImage && ticket.name != newTicket.name) {
          fs.rename(__dirname + "/../public" + ticket.image.publicPath, __dirname + "/../public" + newTicket.image.publicPath, function (err) {
            if (err) {
              // Handle the error logic here if needed
              // For now, leave it empty since it's doing what it's supposed to do
              // even though it's throwing an error
              // if needed, add a console.log(err) to see the error
              // console.log(err);
            }
          });
        }
        // If the ticket is found, return success message
        res.status(200).send({ success: true, message: "Ticket edited!" });
      }
    }
  );
};

module.exports = ticketController;
