var Administrator = require("../models/Administrator");
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
var dotenv = require("dotenv");
dotenv.config({ path: "./config.env" });

var authController = {};

authController.formLogin = function (req, res) {
  res.render("login", { error: false });
};

authController.errorLogin = function (req, res) {
  res.render("login", { error: true });
};

authController.login = function (req, res) {
  Administrator.findOne({ email: req.body.email }).then((query) => {
    if (query != null) {
      var admin = query["_doc"];
    }

    // Verify if the user exists
    if (!admin) {
      return res.redirect("/admin/login/error");
    }

    // Verify if the password introduced is correct
    var passwordIsValid = bcrypt.compareSync(req.body.password, admin.password);

    // If the password is not correct, we redirect the user to the login page with an error message
    if (!passwordIsValid) {
      return res.redirect("/admin/login/error");
    }

    // If the password is correct, we create a token and redirect the user to the dashboard with the respective permissions
    var token = jwt.sign(
      { id: admin._id, role: admin.administratorType },
      `${process.env.SECRET}`,
      {
        expiresIn: 86400, // expires in 24 hours
      }
    );

    res.cookie("token", token);

    res.cookie("name", admin.name);

    res.redirect("/admin");
  });
};

authController.verifyToken = function (req, res, next) {
  // If the user has a token in the cookies, we use that token, otherwise we use the token in the headers
  // since the user might be using an HTTP request instead of a browser
  // Functions like Edit and Delete use HTTP requests instead of the browser
  var token = req.cookies.token
    ? req.cookies.token
    : req.headers["x-access-token"];

  if (!token) {
    return res.render("login", { error: false });
  }

  jwt.verify(token, `${process.env.SECRET}`, function (err, decoded) {
    // If the token is not valid, we redirect the user to the login page so they can login again
    if (err) {
      return res.render("login", { error: false });
    }

    req.userId = decoded.id;
    next();
  });
};

authController.verifyTokenManager = function (req, res, next) {
  // If the user has a token in the cookies, we use that token, otherwise we use the token in the headers
  // since the user might be using an HTTP request instead of a browser
  // Functions like Edit and Delete use HTTP requests instead of the browser
  var token = req.cookies.token
    ? req.cookies.token
    : req.headers["x-access-token"];

  if (!token) {
    return res.render("login", { error: false });
  }

  jwt.verify(token, `${process.env.SECRET}`, function (err, decoded) {
    // If the token is not valid, we redirect the user to the login page so they can login again
    if (err || decoded.role != "Manager") {
      return res.render("login", { error: false });
    }

    req.userId = decoded.id;
    next();
  });
};

authController.logout = function (req, res) {
  res.clearCookie("token");
  res.clearCookie("name");
  res.redirect("/admin");
};

authController.dashboard = function (req, res) {
  res.render("template", {
    loadContent: {
      page: "dashboard.ejs",
    },
  });
};

module.exports = authController;
