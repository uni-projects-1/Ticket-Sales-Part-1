var mongoose = require("mongoose");
var Administrator = require("../models/Administrator.js");
var bcrypt = require("bcryptjs");

var administratorController = {};

administratorController.dashboard = function (req, res) {
  // Find all administrators in the database
  Administrator.find({}).then((dbAdministrators) => {
    if (dbAdministrators == null) {
      // If there is an error, print a message and return an error
      return res.status(500).send({
        success: false,
        message: "Error retrieving administrators",
      });
    } else {
      // If there is no error, render the dashboard page
      dbAdministrators.sort((a, b) => a.administratorId - b.administratorId);
      res.render("../views/template", {
        loadContent: {
          page: "../views/administrator/dashboardAdmin.ejs",
          administrators: dbAdministrators,
        },
      });
    }
  });
};

// Function to render the administrator creation form
administratorController.formCreate = function (req, res) {
  res.render("../views/template", {
    loadContent: {
      page: "../views/administrator/createAdministrator.ejs",
      error: false,
    },
  });
};

administratorController.formCreateError = function (req, res) {
  res.render("../views/template", {
    loadContent: {
      page: "../views/administrator/createAdministrator.ejs",
      error: true,
    },
  });
};

administratorController.create = function (req, res) {
  // Count the number of administrators in the database
  Administrator.countDocuments({}).then((countAdministrators) => {
    debugger;
    // Hash the password
    let hashedPassword = bcrypt.hashSync(req.body.password, 10);

    let administrator = new Administrator({
      // Administrator ID
      administratorId: countAdministrators + 1,
      // Name of the administrator
      name: req.body.name,
      // Gender of the administrator
      gender: req.body.gender,
      // Date of birth of the administrator
      dob: req.body.dob,
      // Cell phone of the administrator
      cellPhone: req.body.cellPhone,
      // Email of the administrator
      email: req.body.email,
      // Street of the administrator's address
      street: req.body.street,
      // City of the administrator's address
      city: req.body.city,
      // Postal code of the administrator's address
      postalCode: req.body.postalCode,
      // Type of the administrator - Administrator or Manager
      administratorType: req.body.administratorType,
      // Password of the administrator
      password: hashedPassword,
    });

    // Check if the administrator ID or email already exists in the database
    Promise.all([
      Administrator.findOne({ administratorId: administrator.administratorId }),
      Administrator.findOne({ email: administrator.email }),
    ]).then(([dbAdministratorId, dbAdministratorEmail]) => {
      if (dbAdministratorId != null || dbAdministratorEmail != null) {
        return res.redirect("http://localhost:5000/admin/administrator/create/error");
      } else {
        administrator.save().then(() => {
          res.redirect("/admin/administrator");
        });
      }
    });
  });
};

administratorController.show = function (req, res) {
  Administrator.findOne({ administratorId: req.params.administratorId }).then(
    (dbAdministrator) => {
      if (dbAdministrator == null) {
        return res.status(500).send({
          success: false,
          message: "Error retrieving administrator",
        });
      } else {
        res.render("../views/template", {
          loadContent: {
            page: "../views/administrator/viewAdministrator.ejs",
            administrator: dbAdministrator,
          },
        });
      }
    }
  );
};

administratorController.formEdit = function (req, res) {
  // Find the administrator by it's ID
  Administrator.findOne({ administratorId: req.params.administratorId }).then(
    function (dbAdministrator) {
      if (dbAdministrator == null) {
        // If he doesn't exist, show an error
        res.render("../views/template", {
          loadContent: {
            page: "../views/administrator/editAdministrator.ejs",
            error: true,
          },
        });
      } else {
        // If he exists, render the editAdministrator.ejs form to edit the administrator
        res.render("../views/template", {
          loadContent: {
            page: "../views/administrator/editAdministrator.ejs",
            administrator: dbAdministrator,
          },
        });
      }
    }
  );
};

administratorController.edit = function (req, res) {
  // Get the previous hashed password of the administrator to compare it with the new one
  let previousPassword = Administrator.findOne({
    administratorId: req.params.administratorId,
  }).then(function (dbAdministrator) {
    if (dbAdministrator == null) {
      //console.log("Error:", err);
    } else {
      return dbAdministrator.password;
    }
  });

  //Checks if the email is valid, in other words, if it's the same as before or new but unique in DB
  let checkDuplicatedEmail = new Promise((resolve, reject) => {
    Administrator.find({ email: req.body.email }).then((administrator) => {
      if (
        (administrator.length != 0 &&
          administrator[0].administratorId == req.params.administratorId) ||
        administrator.length == 0
      ) {
        resolve(); //The email is valid
      } else {
        reject(); //The email cannot be used
      }
    });
  });

  checkDuplicatedEmail
    .then(() => {
      // If the password isn't different from the previous one, it will already be a hashed password so it can directly be added to the database object
      if (req.body.password != previousPassword) {
        // If the password is different from the previous one, hash it and save it in the password variable of the body to be saved in the database
        req.body.password = bcrypt.hashSync(req.body.password, 10);
      }

      let newAdministrator = new Administrator(
        {
          // Administrator ID
          administratorId: req.params.administratorId,
          // Name of the administrator
          name: req.body.name,
          // Gender of the administrator
          gender: req.body.gender,
          // Date of birth of the administrator
          dob: req.body.dob,
          // Cell phone of the administrator
          cellPhone: req.body.cellPhone,
          // Email of the administrator
          email: req.body.email,
          // Street of the administrator's address
          street: req.body.street,
          // City of the administrator's address
          city: req.body.city,
          // Postal code of the administrator's address
          postalCode: req.body.postalCode,
          // Type of the administrator - Administrator or Manager
          administratorType: req.body.administratorType,
          // Password of the administrator
          password: req.body.password,
        },
        { _id: false }
      );

      // Find the administrator by its ID and update it with the new information
      Administrator.findOneAndUpdate(
        { administratorId: req.params.administratorId },
        newAdministrator
      ).then((administrator) => {
        if (administrator == null) {
          return res.status(500).send({
            success: false,
            message: "Error editing administrator!",
          });
        } else {
          res.status(200).send({
            success: true,
            message: "Administrator edited!",
          });
        }
      });
    })
    .catch(() => {
      res.status(500).send({
        success: false,
        message: "Email already in use by another administrator!",
      });
    });
};

decrementId = function (req, res, next) {
  return new Promise((resolve, reject) => {
    Administrator.find({
      administratorId: { $gt: req.params.administratorId },
    }).then((administrators) => {
      administrators.forEach((administrator) => {
        administrator.administratorId = administrator.administratorId - 1;
        administrator.save();
      });
      resolve();
    });
  });
};

administratorController.delete = function (req, res, next) {
  Administrator.findOneAndDelete({
    administratorId: req.params.administratorId,
  }).then(async (administrator) => {
    if (administrator == null) {
      return res.status(500).send({
        success: false,
        message: "Administrator not found",
      });
    } else {
      await decrementId(req, res, next);
      return res.status(200).send({
        success: true,
        message: "Administrator deleted successfully!",
      });
    }
  });
};

module.exports = administratorController;
