1. Click BackOffice
2. Login (email:manager@manager.com ps:admin)
3. Open sidenav
4. Click Staff
4.1. Create Staff
4.2. View Staff
4.3. Remove Staff
5. Click Points
5.1. Creat Point System
5.2. Remove Point System
6. Click Venues
6.1. Create Venue
6.2. View Venue
6.3. Remove Venue (If Events == 0)
7. Click Events
7.1. Create Event
7.2. View Event
7.3. Remove Event (If Tickets == 0)
8. Click Clients
8.1. Create Clients
8.2. View Clients
8.3. Remove Clients (If Sales == 0)
9. Click Tickets
9.1. Create Ticket
9.2. View Ticket
9.3. Remove Ticket (If Sales == 0)
10. Click Sales
10.1. Create Sale
10.2. View Sale