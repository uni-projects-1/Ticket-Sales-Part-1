let ticketInfo = document.getElementById("ticketInfo");

let row3 = document.createElement("div");
row3.setAttribute("class", "row");
let row4 = document.createElement("div");
row4.setAttribute("class", "row");
let col10 = document.createElement("div");
col10.setAttribute("class", "col-lg-6 col-md-12");
let col11 = document.createElement("div");
col11.setAttribute("class", "col-lg-6 col-md-12");
let col12 = document.createElement("div");
col12.setAttribute("class", "col-lg-6 col-md-12");
let col13 = document.createElement("div");
col13.setAttribute("class", "col-lg-6 col-md-12");

// Create the labels for the fields
let priceVipTicket = document.createElement("label");
priceVipTicket.innerHTML = "Price of each VIP Ticket";
let priceFamilyTicket = document.createElement("label");
priceFamilyTicket.innerHTML = "Price of each Family Ticket";
let priceNormalTicket = document.createElement("label");
priceNormalTicket.innerHTML = "Price of each Normal Ticket";
let priceChildTicket = document.createElement("label");
priceChildTicket.innerHTML = "Price of each Child Ticket";

// Create the input fields
let vipTicket = document.createElement("input");
vipTicket.setAttribute("type", "text");
vipTicket.setAttribute("class", "form-control");
vipTicket.setAttribute("name", "vipTicketDisplay");
vipTicket.setAttribute("readonly", "true");
vipTicket.setAttribute("required", "true");
let familyTicket = document.createElement("input");
familyTicket.setAttribute("type", "text");
familyTicket.setAttribute("class", "form-control");
familyTicket.setAttribute("name", "familyTicketDisplay");
familyTicket.setAttribute("readonly", "true");
familyTicket.setAttribute("required", "true");
let normalTicket = document.createElement("input");
normalTicket.setAttribute("type", "text");
normalTicket.setAttribute("class", "form-control");
normalTicket.setAttribute("name", "normalTicketDisplay");
normalTicket.setAttribute("readonly", "true");
normalTicket.setAttribute("required", "true");
let childTicket = document.createElement("input");
childTicket.setAttribute("type", "text");
childTicket.setAttribute("class", "form-control");
childTicket.setAttribute("name", "childTicketDisplay");
childTicket.setAttribute("readonly", "true");
childTicket.setAttribute("required", "true");

// Append the labels and the fields to the columns
col10.appendChild(priceVipTicket);
col10.appendChild(vipTicket);
col11.appendChild(priceNormalTicket);
col11.appendChild(normalTicket);
col12.appendChild(priceFamilyTicket);
col12.appendChild(familyTicket);
col13.appendChild(priceChildTicket);
col13.appendChild(childTicket);

// Append the columns to the rows
row3.appendChild(col10);
row3.appendChild(col11);

row4.appendChild(col12);
row4.appendChild(col13);

// Append the rows to the form
ticketInfo.appendChild(row3);
ticketInfo.appendChild(row4);

// We want to make all childNodes of the ticketInfo div hidden
ticketInfo.childNodes.forEach((element) => {
  element.setAttribute("hidden", "true");
});

document.getElementById("btnSearchTicket").addEventListener("click", () => {
  var selectElement = document.getElementById("eventChosen");
  var selectedOption = selectElement.options[selectElement.selectedIndex];
  var valueSelected = selectedOption.innerText;
  const url = "/admin/ticket/getTicketsByEventName/" + valueSelected;

  // Request the tickets from the server and
  // create the div with information about the tickets
  fetch(url).then(async (response) => {
    let responseBody = await response.json();
    //console.log(responseBody);
    // Sort the tickets by price
    // We want to display the most expensive tickets first - VIP, Normal, Family, Child
    responseBody.sort((a, b) => {
      return b.price - a.price;
    });

    // Set the values of the input fields
    vipTicket.setAttribute("value", responseBody[0].price + "€");
    normalTicket.setAttribute("value", responseBody[1].price + "€");
    familyTicket.setAttribute("value", responseBody[2].price + "€");
    childTicket.setAttribute("value", responseBody[3].price + "€");

    // We want to make all childNodes of the ticketInfo div visible when we get the tickets
    ticketInfo.removeAttribute("hidden");
    ticketInfo.childNodes.forEach((element) => {
      element.removeAttribute("hidden");
    });
  });
});

const observer = new MutationObserver((mutationsList, observer) => {
  // We want to know when the event information is displayed
  // so we can display the ticket input fields
  // with the amount of tickets to be sold for that event
  if (!ticketInfo.hasAttribute("hidden")) {
    // Row 1 has the VIP and Family tickets input fields
    let row5 = document.createElement("div");
    row5.setAttribute("class", "row");
    let col14 = document.createElement("div");
    col14.setAttribute("class", "col-lg-6 col-md-12");
    let col15 = document.createElement("div");
    col15.setAttribute("class", "col-lg-6 col-md-12");

    // Create the labels for the fields
    let labelVipTicket = document.createElement("label");
    labelVipTicket.innerHTML = "Amount of VIP Tickets";
    let labelNormalTicket = document.createElement("label");
    labelNormalTicket.innerHTML = "Amount of Normal Tickets";

    // Create the input fields
    let vipTicket = document.createElement("input");
    vipTicket.setAttribute("type", "number");
    vipTicket.setAttribute("class", "form-control");
    vipTicket.setAttribute("id", "vipTicket");
    vipTicket.setAttribute("name", "vipTicket");
    vipTicket.setAttribute("min", "0");
    vipTicket.setAttribute("max", "100");
    vipTicket.setAttribute("value", "0");
    vipTicket.setAttribute("required", "true");

    let normalTicket = document.createElement("input");
    normalTicket.setAttribute("type", "number");
    normalTicket.setAttribute("class", "form-control");
    normalTicket.setAttribute("id", "normalTicket");
    normalTicket.setAttribute("name", "normalTicket");
    normalTicket.setAttribute("min", "0");
    normalTicket.setAttribute("max", "100");
    normalTicket.setAttribute("value", "0");
    normalTicket.setAttribute("required", "true");

    // Append the labels and the fields to the columns
    col14.appendChild(labelVipTicket);
    col14.appendChild(vipTicket);
    col15.appendChild(labelNormalTicket);
    col15.appendChild(normalTicket);

    // Row 2 has the Normal and Child tickets input fields
    let row6 = document.createElement("div");
    row6.setAttribute("class", "row");
    let col16 = document.createElement("div");
    col16.setAttribute("class", "col-lg-6 col-md-12");
    let col17 = document.createElement("div");
    col17.setAttribute("class", "col-lg-6 col-md-12");

    // Create the labels for the fields
    let labelFamilyTicket = document.createElement("label");
    labelFamilyTicket.innerHTML = "Amount of Family Tickets";
    let labelChildTicket = document.createElement("label");
    labelChildTicket.innerHTML = "Amount of Child Tickets";

    // Create the input fields
    let familyTicket = document.createElement("input");
    familyTicket.setAttribute("type", "number");
    familyTicket.setAttribute("class", "form-control");
    familyTicket.setAttribute("id", "familyTicket");
    familyTicket.setAttribute("name", "familyTicket");
    familyTicket.setAttribute("min", "0");
    familyTicket.setAttribute("max", "100");
    familyTicket.setAttribute("value", "0");
    familyTicket.setAttribute("required", "true");

    let childTicket = document.createElement("input");
    childTicket.setAttribute("type", "number");
    childTicket.setAttribute("class", "form-control");
    childTicket.setAttribute("id", "childTicket");
    childTicket.setAttribute("name", "childTicket");
    childTicket.setAttribute("min", "0");
    childTicket.setAttribute("max", "100");
    childTicket.setAttribute("value", "0");
    childTicket.setAttribute("required", "true");

    // Append the labels and the fields to the columns
    col16.appendChild(labelFamilyTicket);
    col16.appendChild(familyTicket);
    col17.appendChild(labelChildTicket);
    col17.appendChild(childTicket);

    // Append the columns to the rows
    row3.appendChild(col10);
    row3.appendChild(col14);
    row4.appendChild(col11);
    row4.appendChild(col15);
    row5.appendChild(col12);
    row5.appendChild(col16);
    row6.appendChild(col13);
    row6.appendChild(col17);

    // Append the rows to the ticketForm div
    ticketInfo.appendChild(row3);
    ticketInfo.appendChild(row4);
    ticketInfo.appendChild(row5);
    ticketInfo.appendChild(row6);

    // Append the rows to the ticketForm div
    /*
    document.getElementById("ticketForm").appendChild(row5);
    document.getElementById("ticketForm").appendChild(row6);
    document.getElementById("ticketForm").removeAttribute("hidden");*/

    // Stop observing after the ticket input fields are displayed
    observer.disconnect();
  }
});

observer.observe(ticketInfo, { attributes: true, attributeFilter: ["hidden"] });
