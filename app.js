var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var bodyParser = require("body-parser");
var cors = require("cors");
var swaggerUi = require("swagger-ui-express");
var swaggerDocument = require("./swagger/swagger.json");
var dotenv = require("dotenv");
dotenv.config({ path: "./config.env" });

var mongoose = require("mongoose");
mongoose.promise = global.Promise;

mongoose
  .connect(
    `mongodb+srv://${process.env.USERNAME_DB}:${process.env.PASSWORD_DB}@paw.aww1rsv.mongodb.net/${process.env.DB_NAME}`,
    { useNewUrlParser: true }
  )
  .then(() => console.log("connection succesful"))
  .catch((err) => console.error(err));

const indexRouter = require("./routes/index");
const authRouter = require("./routes/authentication");
const clientRouter = require("./routes/clients");
const salesRouter = require("./routes/sales");
const patrimonyRouter = require("./routes/patrimony");
const eventsRouter = require("./routes/events");
const pointsRouter = require("./routes/points");
const ticketsRouter = require("./routes/tickets");
const administratorRouter = require("./routes/administrator");
const authController = require("./controllers/Auth");

const clientStoreRouter = require("./routes/store/clientStore");
const eventStoreRouter = require("./routes/store/eventStore");
const patrimonyStoreRouter = require("./routes/store/patrimonyStore");
const salesStoreRouter = require("./routes/store/salesStore");
const ticketsStoreRouter = require("./routes/store/ticketsStore");
const pointStoreRouter = require("./routes/store/pointStore");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/admin", authRouter);
app.use("/admin/patrimony", authController.verifyToken, patrimonyRouter);
app.use(
  "/admin/event", authController.verifyToken,
  eventsRouter
);
app.use("/admin/point", authController.verifyToken, pointsRouter);
app.use("/admin/ticket", authController.verifyToken, ticketsRouter);
app.use(
  "/admin/administrator",
  authController.verifyToken,
  administratorRouter
);
app.use("/admin/client", authController.verifyToken, clientRouter);
app.use("/admin/sale", authController.verifyToken, salesRouter);

app.use("/api/v1/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use("/store/client", clientStoreRouter);
app.use("/store/event", eventStoreRouter);
app.use("/store/patrimony", patrimonyStoreRouter);
app.use("/store/sale", salesStoreRouter);
app.use("/store/ticket", ticketsStoreRouter);
app.use("/store/point", pointStoreRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

console.log("http://localhost:5000/");

module.exports = app;
